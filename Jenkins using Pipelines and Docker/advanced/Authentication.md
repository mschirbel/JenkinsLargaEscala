# Authentication & Security

Há algumas boas práticas para a segurança do Jenkins:

1. Não deixe o Jenkins exposto na internet
2. Use um firewall(não esqueça de colocar o IP do que você precisa numa whitelist)
3. Deixe o Jenkins atrás de uma VPN
4. Atualize o Jenkins usando sempre a LTS
5. Atualize os plugins
6. Configure a autenticação
7. Dê acessos e permissões corretas aos usuários

## Authentication & Authorization

Authentication: É um processo de verificar a identidade de um usuário.
Authorization: É dar os acessos corretos a recursos para determinados usuários.

## Authorization

Temos diversas opções:

    - Todos podem fazer tudo - não é recomendado
    - Quem está logado pode fazer tudo
    - Matriz de Autorização
    - Role Strategy Plugin - um pouco mais detalhado que a Matriz

Essa Matriz é uma tabela com os as permissões de cada usuário.

### I've locket myself out

Quando isso acontecer:

1. Edite o arquivo de configuração do Jenkins

``` 
docker stop jenkins
vim /var/jenkins_home/config.xml
```

2. Dê acesso de admin ao seu usuário:

```
<autorizationStrategy
class="hudson.security.ProjectMaxtrizAuthorizationStrategy">
<permission>hudson.model.Hudson.Administer:USUÁRIO</permission>
</autorizationStrategy>
```

3. Troque as permissões do seu usuário usando a UI.

```
docker start jenkins
```

### Authorization Considerations

Ao levantar um firewall para o seu Jenkins, lembre-se que o Jenkins exporta duas portas para o Host:

- 8080, que é a porta usada pela UI
- 5000, que pode default é a porta do JNLP(pode ser trocada)

Na parte de **Configure Global Security** podemos escolher qual método de Authorization desejamos ter no nosso sitema.

E a Matriz de Autorização é assim:

![](../images/Screenshot-from-2019-05-25-14.38.53.png)

*Não deixe o seu usuário sem acesso admin, pois assim você ficará lockado para fora do Jenkins*.

## Authentication

Jenkins tem o próprio método de autenticação. Mas seria melhor termos um outro local, até mesmo mais seguro, para fazer essa administração.

Pode ser um banco de dados comum, ou até mesmo um LDAP(AD group). O uso de um LDAP pode ser feito em **Configure Global Security** dentro de **Manage Jenkins**.

Um solução além do AD é o Onelogin. É uma solução [online](https://onelogin.com), que é usada por muitas empresas.

O Onelogin é feito em SAML, parecido com YAML, mas feito especalmente para autenticação e troca de dados entre partes. E ainda tem MFA. Ao usar o Onelogin, instale o Plugin **SAML Plugin**.