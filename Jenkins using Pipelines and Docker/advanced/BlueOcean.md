# Blue Ocean

É um novo frontend para o Jenkins. Com o tempo, vai substituir a antiga UI.

No momento, é tratado como um plugin, mas com o tempo vai ser nativo.

Em **Manage Jenkins** instale o plugin do *Blue Ocean*. Uma vez instalado, haverá um botão *Open Blue Ocean* no centro da tela.

Agora temos mais integrações com Pull Requests e Branches do sistema de versionamento, que será nativo no futuro.