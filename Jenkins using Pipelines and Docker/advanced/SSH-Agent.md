# SSH Agent

Quando se tem Slaves, eles precisam de acesso aos repositórios. E não é boa prática armazenar *keys* ou senhas nos Slaves. Elas devem ser mantidas somente no Master.

Quando temos o problema de SSH keys, podemos usar um SSH Agente para solucionar o problema no Slave.

O SSH Agent funciona no Master e contém as *keys* para acesso em sistemas externos. Como por exemplo Github ou Bitbucket.

Caso use repositórios públicos, basta ter cadastrado as credenciais de login.
Mas se estiver usando algum privado, você deve ter a credencial de SSH também.

## Install

Devemos instalar o plugin **SSH Agent Plugin**.

Ele serve para acessarmos serviços externos *de dentro* do Jenkinsfile. Podemos ver um exemplo abaixo:

```groovy
node {
  stage('do something with git') {  
    sshagent (credentials: ['github-key']) {
      // get the last commit id from a repository you own
      sh 'git ls-remote -h --refs git@github.com:wardviaene/jenkins-course.git master |awk "{print $1}"'
    }
  }
}
```

Muito provavelmente, devemos inserir o host remoto na lista de conhecidos, isso pode ser feito com o comando:

```sh
ssh-keyscan -p <port> <public-ip> >> <JENKINS_HOME>/.ssh/known_hosts
```