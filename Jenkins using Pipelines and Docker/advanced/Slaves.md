# Slave Architecture

Em ambientes de produção não é boa prática ter somente um node para o Jenkins.
É interessante termos um ambiente pequeno para o Master e diversos Workers respondendo aos Jobs do Master.

Assim, podemos expandir a nossa capacidade de *builds*.

E o Jenkins entende que o número de builds correntes é proporcional ao número de Workers(Slaves), ou seja, se houverem mais builds, elas estarão em *queue*.

Jenkins tem dois tipo de *scaling* para os slaves:

1. manual - podemos adicionar mais slaves conformes temos a necessidade, ou em determinadas horas do dia
2. dinâmico - plugins que fazem o scaling, como o **Amazon EC2 Plugin**, que usa a API do EC2 para subir novos workers conforme a necessidade da *queue*. Ou outro exemplo é o **Docker Plugin** que sobe um novo container para um novo slave.

As *builds* podem ser executadas em nodes específicos, isso pode ser configurado na UI ou via Jenkinsfile.

## Benefits

1. reduzir os custos, pois só vamos pagar o que estamos usando;
2. slaves são efêmeros;
3. o overload de CPU e de memória não afeta o Master;
4. respondemos facilmente a um overload de builds na queue;

## Best Practices

1. tenha um padrão nos slaves;
2. nunca instale coisas manualmente;
3. use sempre os plugins;
4. sempre devemos construir e destruir os slaves

## Model using SSH

Nesse modelo, usaremos SSH para o Master se comunicar com os Slaves.

Dentro da nossa UI, temos a opção **Manage Nodes** dentro de **Manage Jenkins**.
Nessa opção temos a opção de adicionar um novo Node.

![](../images/Screenshot-from-2019-05-24-14.11.08.png)

Perceba que, podemos usar esse node quando quisermos, ou apenas para Jobs indicando que ele deve ser usado.

É obrigatório o uso de um diretório para armazenamento das informações desse Node.
Bem como o módo de criação:

![](../images/Screenshot-from-2019-05-24-14.20.29.png)

No nosso caso, usaremos a criação via SSH. E para isso que isso aconteça, precisamos inserir o host que será usado para criar o Node bem como o método de login.

Isso vai variar dependendo do cloud vendor ou até mesmo das máquinas virtuais usadas. É recomendado usar uma key para fazer esse login ao invés de uma senha.

Para criar o container do Slave, temos os arquivos em *resources/jenkins-slave/*

O Dockerfile do Slave:

```dockerfile
FROM openjdk:8-jdk

# install git, curl, openssh server, and remove host keys
RUN apt-get update && apt-get install -y git curl openssh-server && mkdir /var/run/sshd && rm -rf /var/lib/apt/lists/* && rm -rf /etc/ssh/ssh_host_*

# prepare home, user for jenkins
ENV JENKINS_HOME /var/jenkins_home

ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000

RUN groupadd -g ${gid} ${group} \
    && useradd -d "$JENKINS_HOME" -u ${uid} -g ${gid} -m -s /bin/bash ${user}

VOLUME /var/jenkins_home

# get docker client
RUN mkdir -p /tmp/download && \
 curl -L https://get.docker.com/builds/Linux/x86_64/docker-1.13.1.tgz | tar -xz -C /tmp/download && \
 rm -rf /tmp/download/docker/dockerd && \
 mv /tmp/download/docker/docker* /usr/local/bin/ && \
 rm -rf /tmp/download && \
 groupadd -g 999 docker && \
 usermod -aG docker jenkins

# expose ssh port
EXPOSE 22

# make sure host keys are regenerated before sshd starts
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

```

E o script para habilitar o openssh-server:

```sh
#!/bin/bash
dpkg-reconfigure openssh-server
/usr/sbin/sshd -D

```

Caso você esteja usando cloud, na aba de *user-data* da criação da instância, pode ser usado o seguinte script:

```bash
#!/bin/bash
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
apt-get update
apt-get install -y docker-engine
systemctl enable docker
mkdir -p /var/jenkins_home/.ssh
cp /root/.ssh/authorized_keys /var/jenkins_home/.ssh/authorized_keys
chmod 700 /var/jenkins_home/.ssh
chmod 600 /var/jenkins_home/.ssh/authorized_keys
chown -R 1000:1000 /var/jenkins_home
docker run -p 2222:22 -v /var/jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --restart always -d wardviaene/jenkins-slave
```

Esse script instala o Docker, cria a key para o login e sobe um container usando o Dockerfile acima.

Caso encontre um erro de *known hosts*, precisamos usar o comando:

```sh
ssh-keyscan -p <port> <public-ip> >> <JENKINS_HOME>/.ssh/known_hosts
```

E faça o *relaunch* do Node.

Caso deseje testar o Node:

```groovy
node(label: 'builder'){
    stage('Preparation'){
        git 'https://githu.com/username/repo.git'
    }
    stage('Build'){
        def = myTestCont = docker.image('node:4.6')
        myTestCont.pull()
        myTestCont.inside {
            sh 'npm install'
        }
    }
}
```

## Model using JNLP

Nesse modelo, usaremos JNLP para o Slave iniciar o contato com o Master.
É particularmente bom para Windows ou se estivermos atrás de um Firewall.

Para entender mais sobre JNLP, clique [aqui](https://www.java.com/pt_BR/download/faq/java_webstart.xml).

Ao criar um novo Node, selecione a opção **Launch this agent via Java Web Start** em *Launch Options*.

No Linux, ao criar o node, o Jenkins exportará um comando para você criar o Worker:

![](../images/Screenshotfrom-2019-05-24-14.59.27.png)

Basta rodar o comando no terminal da máquina do Node.
Não se esqueça de instalar o Java e pegar o *slave.jar* do Master

```
apt install opendjk-8
wget <ipMaster>:8080/jnlpJars/slave.jar
```
Não se esqueça que, ao bootar a máquina do Node, devemos rodar o comando que foi fornecido pelo Jenkins.

No Windows, basta usar a URL da UI do Master e clicar no Botão laranja **Launch**.