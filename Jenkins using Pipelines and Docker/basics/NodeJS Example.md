# NodeJS App

Vamos ver como construir e como fazer o deploy de uma node app.

## What is Node?

NodeJS é um *runtime environment* que é usado para executar JS como server side.

Também é assíncrono, o que acelera o processo de server side.

E é bem fácil de entender e não precisa ser compilado, como Java.

## How to build

Precisamos primeiro instalar o Node Environment. Podemos baixar [aqui](https://nodejs.org/en/download/).

Depois instalamos o docker e fazemos a nossa imagem.

## Demo

Após instalar o Jenkins, use os recursos em *resources/docker-demo-master/*.

Ali temos uma NodeJS app que nos mostra o host e a porta que estamos conectados, algo bem simples.
Também mostrará um Hello World.

```js
var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('Hello World!');
});

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});

```

Temos um *package.json* para instalar algumas dependências. 

No nosso Dockerfile iremos construir nossa imagem do Docker usando essa aplicação.

```Dockerfile
FROM node:4.6
WORKDIR /app
ADD . /app
RUN npm install
EXPOSE 3000
CMD npm start

```

### Without Docker

Na Dashboard do Jenkins, vá para o **Manage Jenkins** e depois em **Manage Plugins**.
Entre na Aba *Avaliable* e procure por Nodejs

![](../images/Anotação-2019-04-28-150217.jpg)

Instale esse plugin e faça o restart do Jenkins.

Agora volte para a Dashboard, clique em **Manage Jenkins**, depois em **Global Configuration Tool**.

E adicione o NodeJS:

![](../images/Anotação-2019-04-28-151622.jpg)

Agora, vamos criar um novo Job. Clique em **create new jobs**, dê um nome e selecione **Freestyle Project**.

![](../images/Anotação-2019-04-28-150855.jpg)

Agora, vamos configurar o Job manualmente.

Na Aba *General* vamos selecionar um repositório git, com uma URL que temos acesso. Caso use HTTPS, certifique-se que o repositório é público, caso use SSH, valide que a key para o acesso esteja válida.

Os dados vêm do git, da branch master:

![](../images/Anotação-2019-04-28-151346.jpg)

Na parte de **Build Environment** adicione o NodeJs.

E na parte de **Build** adicione o comando shell:

```sh
npm install
```

Agora, clique **Build Now** no projeto. Assim, poderemos ver o log de nosso Job.

## With Docker

Temos que instalar um plugin para usarmos o Docker. Nesse caso, usamos o plugin **Docker Build and Publish**.

Faremos do mesmo jeito que fizemos com o plugin do Node. Escolha qual plugin achar melhor.

Entre no mesmo Job que usamos acima.

Na aba de Configurações do Job, adicione um Build Step:

![](../images/Anotação-2019-04-28-155802.jpg)

*Podemos instalar o Docker via plugin, também. Mas devemos indicar isso em Global Configuration Tool*.

Agora, basta fazer o *build*.

Para acessar a aplicação, basta acessar

```
http://localhost:3000
```