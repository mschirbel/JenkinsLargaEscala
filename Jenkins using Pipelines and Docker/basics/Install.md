# Installing Jenkins

Existem alguns métodos, mas o mais fácil é utilizar o Docker para instalar o Jenkins.

É possível subir o Docker na nuvem ou em qualquer máquina.

Podemos também subir uma VM, e instalar no SO da máquina, podemos usar até o Vagrant para isso.

Jenkins é escrito em Java, ou seja, funciona em qualquer SO. E os Slaves, podem ser usados em Windows, também.

## Using Docker

Para instalar o Jenkins usando o Docker:

```
mkdir jenkins_home
chown -R 1000:1000 jenkins_home
docker pull jenkins/jenkins:lts
docker run -p 8080:8080 -p 50000:50000 jenkins --name jenkins -v /var/jenkins_home:jenkins_home
```

Quando rodar esse comando, o output será a senha de admin para a console administrativa do jenkins.
A console pode ser acessada em *http://localhost:8080*.

Caso tenha perdido a senha, use o comando:

```
cat /var/jenkins_home/secrets/initialAdminPassword
```

Use os plugins sugeridos e aguarde a instalação terminar.

Agora, vamos criar o primeiro admin, dê um nome e uma senha. E podemos usar o Jenkins.

## Using Tomcat

Primeiro, precisamos do Tomcat, aqui, vamos usar a versão 8.0.23:

```
cd /opt
wget http://archive.apache.org/dist/tomcat/tomcat-8/v8.0.23/bin/apache-tomcat-8.0.23.tar.gz
tar -xvfz apache-tomcat-8.0.23.tar.gz
cd apache-tomcat-8.0.23
cd webapps
wget http://ftp-nyc.osuosl.org/pub/jenkins/war/2.174/jenkins.war
cd ../bin/
./startup.sh
```

Acesse usando:

```
http://localhost:8080/jenkins
```