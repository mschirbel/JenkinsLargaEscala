# Pipelines

Pipelines permitem construir os *steps* de uma *build* em formato de código.

Fazer isso, permite que compilemos, testemos e apliquemos o código. Tudo isso, versionando nossos arquivos.

## Pipelines vs DSL

Ambos são códigos de CI/DI, o que difere são as implementações.

DSL criam novos Jobs, conforme escrevemos os códigos.

Pipelines é um **tipo** de Job. E podemos ter diferentes pipelines dentro do mesmo Job.
Isso permite também automatizar novas mudanças no código em diferentes repositórios.

Podemos escrever o Pipeline em:

1. Jenkins DSL - declarativo
2. Groovy - script

Groovy roda na JVM e é similar a Java. DSL também vira Groovy no fim das contas, mas ok.

## Example

```groovy
node{
    def mvnHome
    stage('Preparation'){
        git 'https://github.com/example/example.git'
        mvnHome = tool 'M3'
    }
    stage('Build'){
        if(isUnix()){
            sh "'${mvnHome}/bin/mvn -Dmaven.test.failure.ignore clean package"
        } else {
            bat(/"${mvnHomee}\bin\mvn" -Dmaven.test.failure.ignore clan package/)
        }
    }
    stage('Results'){
        junit '**/target/surefire-reports/TEST-*.xml'
        archive 'target/*.jar'
    }
}
```

Na primeira linha, temos *node*. Isso representa que queremos executar somente nos Nodes do Jenkins. Como aqui só temos uma instância, ele será executado no *Master* mesmo.

Podemos declarar variáveis, como *mvnHome*.

E depois temos os *builds stages*, que são os passos que o Jenkins vai fazer.

## Example with Nodejs and Docker

```groovy
node {
   def commit_id
   stage('Preparation') {
     checkout scm
     sh "git rev-parse --short HEAD > .git/commit-id"                        
     commit_id = readFile('.git/commit-id').trim()
   }
   stage('test') {
     nodejs(nodeJSInstallationName: 'nodejs') {
       sh 'npm install --only=dev'
       sh 'npm test'
     }
   }
   stage('docker build/push') {
        docker.withRegistry('https://index.docker.io/v1/', 'dockerhub') {
            def app = docker.build("wardviaene/docker-nodejs-demo:${commit_id}", '.').push()
        }
   }
}
```

Nesse exemplo, temos 3 *stages*.

Podemos rodar esse Job em qualquer node.

Temos uma variável chamada *commit_id* para servir como id único ao fazermos o push para o dockerhub.

Primeiro, fazemos um git clone do projeto e pegamos o *commit_id* nas seguintes linhas:

```
sh "git rev-parse --short HEAD > .git/commit-id"                        
commit_id = readFile('.git/commit-id').trim()
```

Depois, instalamos as dependências necessárias, no *stage* test.

Por último, logamos no dockerhub, com as credenciais que criamos dentro da GUI do Jenkins. E fazemos o push de nossa imagem

```
def app = docker.build("wardviaene/docker-nodejs-demo:${commit_id}", '.').push()
```

É sempre interessante o arquivo do Jenkinsfile ficar na *root folder* do projeto. Facilita as coisas para os plugins.

Para rodar isso, basta criar um novo Pipeline, indicando o repositório git do projeto.

## Example 2 - Docker, NodeJS and MySQL

```groovy
node {
   def commit_id
   stage('Preparation') {
     checkout scm
     sh "git rev-parse --short HEAD > .git/commit-id"
     commit_id = readFile('.git/commit-id').trim()
   }
   stage('test') {
     def myTestContainer = docker.image('node:4.6')
     myTestContainer.pull()
     myTestContainer.inside {
       sh 'npm install --only=dev'
       sh 'npm test'
     }
   }
   stage('test with a DB') {
     def mysql = docker.image('mysql').run("-e MYSQL_ALLOW_EMPTY_PASSWORD=yes") 
     def myTestContainer = docker.image('node:4.6')
     myTestContainer.pull()
     myTestContainer.inside("--link ${mysql.id}:mysql") {
          sh 'npm install --only=dev' 
          sh 'npm test'                     
     }                                   
     mysql.stop()
   }                                     
   stage('docker build/push') {            
     docker.withRegistry('https://index.docker.io/v1/', 'dockerhub') {
       def app = docker.build("wardviaene/docker-nodejs-demo:${commit_id}", '.').push()
     }                                     
   }                                       
}  
```

Nesse exemplo, temos um estágio a mais, um *test with a DB*.

E temos agora, como fazer testes dentro de containers isolados. Como por exemplo para o node:

```groovy
def myTestContainer = docker.image('node:4.6')
     myTestContainer.pull()
     myTestContainer.inside {
       sh 'npm install --only=dev'
       sh 'npm test'
     }
```

No qual criamos um container com uma versão do Node diferente, simplesmente para fazermos os testes necessários.

E para o MySQL, no qual criamos um container para testarmos o nosso código:

```groovy
def mysql = docker.image('mysql').run("-e MYSQL_ALLOW_EMPTY_PASSWORD=yes") 
     def myTestContainer = docker.image('node:4.6')
     myTestContainer.pull()
     myTestContainer.inside("--link ${mysql.id}:mysql") {
          sh 'npm install --only=dev' 
          sh 'npm test'                     
     }                                   
     mysql.stop()
```

Agora basta replicar os passos para criar um novo pipeline.