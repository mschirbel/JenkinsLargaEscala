# What is Jenkins

É uma ferramenta de CI/CD feita em Java, feito para criar e entregar softwares. É Open Source, também.

Jenkins é baseado em plugins para executar as diversas tarefas que é capaz.

## CI/CD

CI = Continuous Integration, é a prática de integrar as cópias dos desenvolvedores em um único projeto.

CD = Continuous Deployment, é entregar esse projeto de forma automatizada, em ciclos, em qualquer momento.

Essas *builds* serão automatizadas para cada commit no sistema de versionamento, ou uma vez por dia.
Quando o desenvolvedor fizer um *push*, a build será testada e entregue pelo Jenkins. O Jenkins não faz *merge* e nem resolve os conflitos, isso deve ser resolvido pelo desenvolvedor.

## Benefits

1. feedback
2. deploy
3. publish
4. automation
5. ci/cd lifecycle

## Software Development Lifecycle

O desenvolvedor vai escrever o código.
O código vai passar por uma *build*. Isso dentro do sistema de versionamento.

Como boas práticas, devem ser usadas diversas *branches* para evitar erros em produção.

Depois disso, teremos os testes. Temos diversos tipos de testes, como por exemplo:

- unitário
- integração
- regressão
- usuário

O próximo passo é o *release*, que é como o software vai ser incorporado na infraestrutura.

Depois é só fazer o deploy para o usuário.

Jenkins pode ser usado para todas essas fases.