# DSL

É um plugin que permite definir jobs de uma forma programática.

É baseado em Groovy, que é parecido com o Java, mas um pouco mais simples. É uma linguagem de scripts.

DSL foi feito para controlar Jobs, principalmente quando temos diversos Jobs, pois teremos:

1. version control
2. history
3. audit
4. job restore

## npm install example

Ao invés de usar a GUI para formar nosso job, podemos usar na versão DSL:

*nodejs.groovy*
```groovy
job('Node JS example'){
    scm{
        git('git://github.com/wardviaene/docker-demo.git'){ node ->
            node / gitConfigName('DSL User')
            node / gitConfigEmail('jenkins-dsl@newtech.academy')
        }
    }
    triggers{
        scm('H/5 * * * *')
    }
    wrappers{
        nodejs('nodejs')
    }
    steps{
        shell("npm install")
    }
}
```

Primeiro definimos um Job, depois usamos:

1. **scm** - aqui definimos o repositório git para ser usado. Os parâmetros são as configurações de conta
2. triggers - aqui definimos quando vamos rodar esse job, no exemplo, será a cada 5min.
3. wrappers - aqui devemos usar o mesmo nome que instalamos em *Configure Tools*
4. steps - aqui temos os passos da automação

## Demo

Primeiramente, devemos instalar o plugin do job DSL:

!(../images/Anotação-2019-04-30-170217.jpg)

Agora, clicamos em Create New Item, na tela principal do Jenkins, e criamos um *Freestyle Project*

![](../images/Anotação-2019-04-30-170455.jpg)

Aqui, podemos pegar o código do github, usando o plugin do Git, ou na aba *Build*, escolher para usar o script DSL.

Caso escolha usar o Git, certifique-se de colocar o path até o script:

![](../images/Anotação-2019-04-30-170758.jpg)

Clique em Save. E aplique o Job.

Ao fazer isso, irá ter um erro:

```
ERROR: script not yet approved for use
```

Isso é um mecanismo de segurança, pois precisamos aprovar o script antes de usá-lo.
O repositório deve ser confiável.

Para isso, vá até **Manage Jenkins**, **In-process Script Approval** e você cerá o script para aprovação. Agora, podemos buildar o Job.

## Demo with Docker & Dockerhub

Faremos o mesmo agora, com o seguinte script:

```groovy
job('NodeJS Docker example') {
    scm {
        git('git://github.com/wardviaene/docker-demo.git') {  node ->
            node / gitConfigName('DSL User')
            node / gitConfigEmail('jenkins-dsl@newtech.academy')
        }
    }
    triggers {
        scm('H/5 * * * *')
    }
    wrappers {
        nodejs('nodejs')
    }
    steps {
        dockerBuildAndPublish {
            repositoryName('wardviaene/docker-nodejs-demo')
            tag('${GIT_REVISION,length=9}')
            registryCredentials('dockerhub')
            forcePull(false)
            forceTag(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}
```

Para ver a documentação, clique [aqui](https://jenkinsci.github.io/job-dsl-plugin)

Nesse site podemos ver os métodos disponíveis para o plugin.

Também precisamos adicionar as credenciais do nosso dockerhub para fazer o push. Para isso, na aba principal do Jenkins, cliquem em **Credentials** e ali adicione uma nova com o username e senha do dockerhub, sendo que, o ID **deve** ser *dockerhub*.