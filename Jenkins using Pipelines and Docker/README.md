# Curso sobre Jenkins' Pipelines

Pode ser encontrado [aqui](https://www.udemy.com/learn-devops-ci-cd-with-jenkins-using-pipelines-and-docker/learn/v4/content)

## Basics

1. install
2. nodejs + docker + jenkins
3. job dsl
4. pipelines

## Integragions

1. email
2. slack
3. github
4. jfrog
5. sonar

## Advanced

1. blue ocean
2. slaves
3. ssh
4. security
5. auth
6. onelogin