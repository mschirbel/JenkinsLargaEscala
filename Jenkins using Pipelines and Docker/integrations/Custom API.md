# Custom API

Pode acontecer de não haver um plugin disponível ou até não haver as funcionalidades que você precisa em um determinado plugin.

Uma solução é escrever o seu próprio plugin. Mas isso demora muito, e é chato. E pode acontecer de nem ter acesso ao Jenkins.

Outra solução, é usar o HTTP Plugin, que faz requests em uma API via http. Assim, podemos escrever o código para fazer essas requisições.

Geralmente esse plugin já vem instalado por default, mas podemos baixar caso seja necessário.

Para o job, usaremos esse Jenkinsfile:

```groovy
// example of custom API
import groovy.json.JsonSlurperClassic 

@NonCPS
def jsonParse(def json) {
    new groovy.json.JsonSlurperClassic().parseText(json)
}

def repo = "username/repository"

def token = httpRequest authentication: 'bitbucket-oauth', contentType: 'APPLICATION_FORM', httpMode: 'POST', requestBody: 'grant_type=client_credentials', url: 'https://bitbucket.org/site/oauth2/access_token'
def accessToken = jsonParse(token.content).access_token
def pr = httpRequest customHeaders: [[maskValue: true, name: 'Authorization', value: 'Bearer ' + accessToken]], url: "https://api.bitbucket.org/2.0/repositories/${repo}/pullrequests"

for (p in jsonParse(pr.content).values) { 
    println(p.source.branch.name)
}
```

Veja que nesse exemplo, utilizamos o Bitbucket, mas podemos usar o Github sem problemas.

Mas o Bitbucket ofecere uma API pronta para o uso.

Na primeira vez que esse Job rodar, ele vai falhar, pois é necessária a aprovação do JsonSlurperClassic.