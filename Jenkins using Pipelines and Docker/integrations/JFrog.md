# JFrog Artifactory

Depois do Jenkins fazer uma imagem para os testes, ele chama isso de *artifact*.

É basicamente um arquivo binário, que pode ser diversas coisas, como por exemplo:

    - docker image
    - .jar
    - .zip
    - .tgz

E diversas outras coisas. Esses *artifacts* precisam ser armazenados. Para isso, temos o JFrog Artifactory, que armazena o resultado de uma build.

É uma boa prática manter todos os *artifacts*. E eles podem ser úteis em caso de rollback, pois guardam todos os estados necessários.

Para fazermos isso, usamos o **JFrog Plugin**.

Isso permite adicionarmos outros passos ao nosso Jenkinsfile, como por exemplo, o envio da build para o JFrog, usando até mesmo branches para dev, homol e prod.

## Demo

Para fazer o download da versão open source, podemos fazer de dois jeitos:

1. Usando o site oficial, que pode ser visto [aqui](https://jfrog.com/)
2. Usar a versão de cloud, usando o Free Trial.

Na versão cloud, podemos escolher qual o vendor.

Depois, devemos baixar o Plugin, **Artifactory Plugin** dentro do Jenkins.

Após logar na conta do JFrog, use o usuário *admin* e a senha que foi enviada por e-mail para acessar o servidor criado.

Para criar um repositório, clique em *Welcome,admin* na página do JFrog e selecione *Gradle*.

![](../images/Screenshot-from-2019-05-23-19-09-50.png)

Por default, vai criar alguns repositórios para controle.
Para melhor controle, crie um grupo para fazer o *deploy*. E insira algum usuário nesse grupo. Não esqueça de colocar as corretas permissões para o grupo.

Agora, dentro da console do Jenkins, vá em **Manage Jenkins** e configure o Artifactory Plugin.
Para isso, selecione um servidor para fazer o deploy. O username será o mesmo usado no site do JFrog. 

Quanto a senha, podemos gerar uma API Key dentro do nosso profile, para usarmos externamente sem afetar nossa conta.

O Server ID vai ser o <domínio>.jfrog.io
E a URL vai ser https://<domínio>.jfrog.io/<domínio>;

Realize o teste de conexão para ver se há comunicação entre as partes.

Para ter um Job de exemplo, use o seguinte:

```groovy
node {
    def server = Artifactory.server('newtechacademy.jfrog.io')
    def rtGradle = Artifactory.newGradleBuild()
    def buildInfo = Artifactory.newBuildInfo()
    stage 'Build'
        git url: 'https://github.com/wardviaene/gs-gradle.git'

    stage 'Artifactory configuration'
        rtGradle.tool = 'gradle' // Tool name from Jenkins configuration
        rtGradle.deployer repo:'gradle-dev-local',  server: server
        rtGradle.resolver repo:'gradle-dev', server: server

        stage('Config Build Info') {
            buildInfo.env.capture = true
            buildInfo.env.filter.addInclude("*")
        }

        stage('Extra gradle configurations') {
            rtGradle.usesPlugin = true // Artifactory plugin already defined in build script
        }
        stage('Exec Gradle') {
            rtGradle.run rootDir: "artifactory/", buildFile: 'build.gradle', tasks: 'clean artifactoryPublish', buildInfo: buildInfo
        }
        stage('Publish build info') {
            server.publishBuildInfo buildInfo
        }
}
```

Para esse job, é necessário ir em **Global Configuration** e instalar o Gradle.
Mas isso varia de job para job, com o que deve ser guardado no artifactory.