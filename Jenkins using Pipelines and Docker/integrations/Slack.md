# Slack Integration

Slack é uma ferramenta de comunicação, parecido com Skype, mas muito melhor, pois tem integrações com diversos softwares e APIs.

Se tornou muito famoso a partir de 2015, sendo usado em todo o mundo.

Essa prática é chamada de ChatOps. ChatOps é uma forma de colaboração entre pessoas, ferramentas, processos e automações em um *workflow* transparente.

E podemos integrar o Jenkins para mandar mensagens relevantes ao nosso chat.

## Demo

Para criar um Workspace, entre no [site do Slack](https://slack.com).

Depois disso, instale o plugin do Slack. O que será usado é o **Slack Notification Plugin**.

![](../images/Anotação-2019-05-18-162934.jpg)

Agora, para configurar o Slack, crie um novo *channel*. E depois clique em *Add an app or custom integration*:

![](../images/Anotação-2019-05-18-163041.jpg)

Procure por WebHook. É uma API genérica, que podemos enviar dados e receber dados.

![](../images/Anotação-2019-05-18-163326.jpg)

Clique em *Add Configuration*. E selecione o *channel* que receberá as mensagens:

![](../images/Anotação-2019-05-18-163419.jpg)

Com isso, receberemos uma URL para enviar o WebHook.

Agora, no Jenkins, clique em **Manage Jenkins** na tela principal.

Procure por **Global Slack Notifier Settings**. Adicione alguma credencial, do tipo Secret Text:

![](../images/Anotação-2019-05-18-163647.jpg)

Para o *Secret*, você deve usar o *token* que está na URL do WebHook.

Depois copie a url para as configurações:

![](../images/Anotação-2019-05-18-163817.jpg)

Em *Base URL* coloque o começo da URL que foi gerada no WebHook.
Em *Team Subdomain* coloque qual o seu *Workspace*.
E coloque qual o canal do slack será usado.

Para criar um Job para testar isso, temos o nosso Jenkinsfile em *resources/slack-notifications/*

```groovy
node {
  try {
    stage('build') {
      println('so far so good...')
    }
    stage('test') {
      println('A test has failed!')
      sh 'exit 1'
    }
  } catch(e) {
    currentBuild.result = "FAILURE";
    slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    throw e;
  }
}
```