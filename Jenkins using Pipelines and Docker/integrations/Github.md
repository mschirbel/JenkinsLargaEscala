# Github Integration

Até agora, sempre adicionamos nossos repositórios manualmente. Mas seria interessante que o Jenkins detectasse automaticamente os novos repositórios.

Para o plugin, usaremos o **Github Branch Source Plugin**. Ele fará um *scan* de todos os repositórios por meio de um Pipeline. Por *default* ele já vem na instalação recomendada do Jenkins.

Ao criar um novo Job, selecionamos um **GitHub Organization**.

![](../images/Anotação-2019-05-18-172041.jpg)

E para as configurações de credenciais, vá até a sua página no Github, vá em *Settings* e depois em *Personal Access Tokens*.

Gere um novo *token* com as seguintes permissões:

![](../images/Anotação-2019-05-18-172304.jpg)

Em **Owner** use o seu username do Github.
Copie esse token e use para as credenciais, com o seu username e o *token* como senha.

Podemos trocar a periodicidade com que o Jenkins vai olhar o repositório por novas mudanças.
Agora basta salvar.