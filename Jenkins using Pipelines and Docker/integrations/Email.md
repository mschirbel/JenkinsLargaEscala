# Email Integration

Com a integração de e-mails, podemos alertar o administrador assim que algo acontecer, podendo ser um sucesso ou uma falha.

Quanto mais cedo for dado o feedback ao desenvolvedor, melhor.

Em todo commit, Jenkins deve produzir uma nova build com todos os testes necessários.

Caso algum teste falhe, devemos notificar o desenvolvedor.

O plugin **Email Extension Plugin** já vem instalado por padrão. Vem no pacote de recomendados. Caso não esteja, basta baixar.

## Configure Email Extension Plugin

Para configurar esse plugin, no menu principal, clique em **Manage Jenkins** e depois em **Configure System**.

Aqui, teremos uma aba para o Extended Email Notification. *Não confunda com o Email Notification*.

## Test

Podemos usar o site [mailtrap.io](https://mailtrap.io). Caso não temos um SMTP Server.

As configurações devem ser assim:

![](../images/Anotação-2019-05-15-204242.jpg)

Agora só precisamos criar um Job que vai nos notificar quando a Build Falhar.

## Job

O Jenkinsfile que usaremos está em *resources/email-notifications/*

```groovy
node {

  // put here the emails you want to send 
  def to = emailextrecipients([
          [$class: 'CulpritsRecipientProvider'],
          [$class: 'DevelopersRecipientProvider'],
          [$class: 'RequesterRecipientProvider']
  ])

  // job
  try {
    stage('build') {
      println('so far so good...')
    }
    stage('test') {
      println('A test has failed!')
      sh 'exit 1' // make an error
    }
  } catch(e) {
    // mark build as failed
    currentBuild.result = "FAILURE";
    // set variables
    def subject = "${env.JOB_NAME} - Build #${env.BUILD_NUMBER} ${currentBuild.result}"
    def content = '${JELLY_SCRIPT,template="html"}'

    // send email
    if(to != null && !to.isEmpty()) {
      emailext(body: content, mimeType: 'text/html',
         replyTo: '$DEFAULT_REPLYTO', subject: subject,
         to: to, attachLog: true )
    }

    // mark current build as a failure and throw the error
    throw e;
  }
}
```

Agora basta criar o Pipeline Job e fazer o build.