# Sonarqube

Sonarqube é um software para testar a qualidade de um código.
Ele verifica diversos aspectos, tais como:

1. bugs
2. vulnerabilidades
3. testes
4. manutenção

É muito comum de se integrar com Jenkins. Dentro do Jenkins é somente um *build step* usando o Sonar-Scanner.

O Scanner manda o resultado para o Sonarqube Server, que por sua vez, analisa o código e usa um banco de dados para guardar o estado da análise.

Para isso, usaremos um Docker-compose para subir outros dois containers. Um com o Scanner outro com o DB.

Para o docker-compose.yml:

```yaml
version: '2'
services:
  jenkins:
    image: jenkins/jenkins:lts
    ports:
      - "8080:8080"
      - "50000:50000"
    networks:
      - jenkins
    volumes:
      - /var/jenkins_home:/var/jenkins_home
      - /var/run/docker.sock:/var/run/docker.sock
  postgres:
    image: postgres:9.6
    networks:
      - jenkins
    environment:
      POSTGRES_USER: sonar
      POSTGRES_PASSWORD: sonarpasswd
    volumes:
      - /var/postgres-data:/var/lib/postgresql/data
  sonarqube:
    image: sonarqube:lts
    ports:
      - "9000:9000"
      - "9092:9092"
    networks:
      - jenkins
    environment:
      SONARQUBE_JDBC_USERNAME: sonar
      SONARQUBE_JDBC_PASSWORD: sonarpasswd
      SONARQUBE_JDBC_URL: "jdbc:postgresql://postgres:5432/sonar"
    depends_on: 
      - postgres

networks:
  jenkins:
```

Isso criará 3 containers, todos interligados por uma rede chamada 'jenkins'.

Para subir os containers, usaremos o comando:

```
docker-compose up -d
```

E para o nosso Job, usaremos o seguinte Jenkinsfile:

```groovy
node {
    def myGradleContainer = docker.image('gradle:jdk8-alpine')
    myGradleContainer.pull()

    stage('prep') {
        git url: 'https://github.com/wardviaene/gs-gradle.git'
    }

    stage('build') {
      myGradleContainer.inside("-v ${env.HOME}/.gradle:/home/gradle/.gradle") {
        sh 'cd complete && /opt/gradle/bin/gradle build'
      }
    }

    stage('sonar-scanner') {
      def sonarqubeScannerHome = tool name: 'sonar', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
      withCredentials([string(credentialsId: 'sonar', variable: 'sonarLogin')]) {
        sh "${sonarqubeScannerHome}/bin/sonar-scanner -e -Dsonar.host.url=http://sonarqube:9000 -Dsonar.login=${sonarLogin} -Dsonar.projectName=gs-gradle -Dsonar.projectVersion=${env.BUILD_NUMBER} -Dsonar.projectKey=GS -Dsonar.sources=complete/src/main/ -Dsonar.tests=complete/src/test/ -Dsonar.language=java"
      }
    }
}
```

Você também pode acessar a sua página do Sonarqube usando o *localhost:9000*. Sendo que 9002 é a porta usada para a comunicão.

Dentro da página do Sonarqube, crie um *token*:

![](../images/Screenshot-from-2019-05-23-21-20-51.png)

Esse *token* será usado para a credencial do sistema do Jenkins.
Apenas crie uma nova credencial global.

Agora vamos configurar o **Global Tool Configuration** para o Sonar Scanner
Somente precisamos adicionar um nome e uma versão do servidor.

No nosso novo Pipeline Project perceba que o Scanner é somente um passo:

```groovy
stage('sonar-scanner') {
      def sonarqubeScannerHome = tool name: 'sonar', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
      withCredentials([string(credentialsId: 'sonar', variable: 'sonarLogin')]) {
        sh "${sonarqubeScannerHome}/bin/sonar-scanner -e -Dsonar.host.url=http://sonarqube:9000 -Dsonar.login=${sonarLogin} -Dsonar.projectName=gs-gradle -Dsonar.projectVersion=${env.BUILD_NUMBER} -Dsonar.projectKey=GS -Dsonar.sources=complete/src/main/ -Dsonar.tests=complete/src/test/ -Dsonar.language=java"
      }
    }
```

Dentro do passo, devemos usar a credencial que criamos antes de rodar o Job.